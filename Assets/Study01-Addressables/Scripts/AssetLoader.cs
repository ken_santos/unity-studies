using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;

public static class AssetLoader
{
    public static async Task CreateAssetAddToList<T>(AssetReference p_reference, List<T> p_completedAssets) where T : Object
    {
        p_completedAssets.Add(await p_reference.InstantiateAsync().Task as T);
    }

    public static async Task CreateAssetsAddToList<T>(List<AssetReference> p_references, List<T> p_completedAssets) where T : Object
    {
        for (int i = 0; i < p_references.Count; i++)    
            p_completedAssets.Add(await p_references[i].InstantiateAsync().Task as T);        
    }

}
