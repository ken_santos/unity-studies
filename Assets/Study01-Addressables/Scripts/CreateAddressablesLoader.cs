using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.ResourceLocations;

public static class CreateAddressablesLoader
{
    public static async Task ByLoadedAddress<T>(IList<IResourceLocation> p_loadedLocations, List<T> p_createdAssets) where T : Object
    {
        for(int i = 0; i < p_loadedLocations.Count; i++)
        {
            T asset = await Addressables.InstantiateAsync(p_loadedLocations[i]).Task as T;
            p_createdAssets.Add(asset);
        }
    }
}
