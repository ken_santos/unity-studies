using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.ResourceManagement.ResourceLocations;

public class AddressableLocationHolder : MonoBehaviour
{
    [SerializeField] private string m_label;

    public List<IResourceLocation> AssetLocations { get; } = new List<IResourceLocation>();

    private void Start()
    {     
        StartCoroutine(InitAndLoadLocation(DebugCallback));
    }

    private IEnumerator InitAndLoadLocation(Action p_callback = null)
    {
        yield return AddressableLocationLoader.GetAll(m_label, AssetLocations);

        p_callback?.Invoke();
    }

    private void DebugCallback()
    {
        for (int i = 0; i < AssetLocations.Count; i++)       
            Debug.Log(AssetLocations[i].PrimaryKey);        
    }
}
