using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.ResourceLocations;

public static class AddressableLocationLoader 
{
    public static async Task GetAll(string p_label, List<IResourceLocation> p_loadedLocations)
    {
        IList<IResourceLocation> unloadedLocations = await Addressables.LoadResourceLocationsAsync(p_label).Task;
        
        for (int i = 0; i < unloadedLocations.Count; i++)
            p_loadedLocations.Add(unloadedLocations[i]);
    }
}
