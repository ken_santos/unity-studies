using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.ResourceLocations;
using UnityEngine.UI;

public class LoadAssetsFromAWS : MonoBehaviour
{
    [SerializeField] private string m_label;
    [SerializeField] private Text m_locationLabel;
    // Start is called before the first frame update
    void Start()
    {
        Get(m_label);
    }

    private async Task Get(string p_label)
    {
        IList<IResourceLocation> locations = await Addressables.LoadResourceLocationsAsync(p_label).Task;

        for (int i = 0; i < locations.Count; i++)
        {
            m_locationLabel.text += (locations[i]) + "\n";
            await Addressables.InstantiateAsync(locations[i]).Task;
        }

    }
}
