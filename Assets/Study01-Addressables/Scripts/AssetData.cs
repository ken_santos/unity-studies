using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class AssetData : MonoBehaviour
{
    [Header("Asset References")]
    [SerializeField] private AssetReference m_cubeReference;
    [SerializeField] private List<AssetReference> m_references = new List<AssetReference>();

    [Header("Game Objects")]
    [SerializeField] private List<GameObject> m_loadedObjects = new List<GameObject>();

    private void Start()
    {
        m_references.Add(m_cubeReference);

        StartCoroutine(LoadAndWaitUntilComplete());
    }

    private IEnumerator LoadAndWaitUntilComplete()
    {
        yield return AssetLoader.CreateAssetsAddToList(m_references, m_loadedObjects);
    }
}
