using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class CreateAssets : MonoBehaviour
{
    private AddressableLocationHolder m_addressableLocations;
    [SerializeField] private List<GameObject> m_assets = new List<GameObject>();

    private void Awake()
    {
        m_addressableLocations = GetComponent<AddressableLocationHolder>();
    }

    private void Start()
    {
        CreateAndWait();
    }

    private async Task CreateAndWait()
    {
        await Task.Delay(TimeSpan.FromSeconds(1));

        await CreateAddressablesLoader.ByLoadedAddress(m_addressableLocations.AssetLocations, m_assets);

        for(int i = m_assets.Count - 1; i >= 0; i--)
        {
            await Task.Delay(TimeSpan.FromSeconds(2));

            Addressables.Release(m_assets[i]);
        }
        m_assets.Clear();
    }

}
